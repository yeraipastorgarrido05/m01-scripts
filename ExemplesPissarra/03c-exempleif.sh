# /bin/bash
# Yerai Pastor Garrido ASIX-M01
# Gener de 2024
#
# Exemples ordre if
# ---------------------------------------------
#
# 1) Validar arguments
if [ $# -ne 1 ]
then
  echo "Error: num args incorrecte"
  echo "Usage: $0 edat"
  exit 1 
fi

# 2) Xixa

edat=$1
if [ $edat -lt 18 ]
then
  echo "edat $edat és menor d'edat"
elif [ $edat -lt 65 ]
  echo " edat $edat es edat activa"
else
  echo "edat $edat és edat de jubilació"

fi
exit 0
