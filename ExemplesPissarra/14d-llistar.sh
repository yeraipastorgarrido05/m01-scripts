# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# programa dir
# a) rep un arg i es un directori i es llista
# b) llistar enumerant els elements del dir
# c) per cada element dir si es dir, regular, o altre cosa
# 
# 14c-llistar.sh
#
# --------------------------------------------------------
# 1) Valida que hi ha un argument

if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
 fi
dir=$1

# 2) Valida que es un dir    
dir=$1
if [ ! -d $dir ]
then
  echo "Error: $1 no es un directori"
  echo "Usage: $0 directori"
  exit $ERR_NODIR
fi

for dir in *
do
  if [ ! -d $dir  ]then
    echo "Error!: $dir  No es un directori" >> /dev/stderr
  else
    
# 3) xixa:llistar

  llista_dir=$(ls $dir)
  do
  for elem in $llista_dir
    if [ -f $dir/$elem ]then
     echo "$elem es un regular file"
  
    elif [ -d $dir/$elem ]then
     echo "$elem es un directori"
  
    else
     echo "$elem es un altra cosa"
fi
done
exit 0



