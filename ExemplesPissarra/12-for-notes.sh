# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Programa que rep almenys una nota o més  i per cada nota diu si és un suspès, aprovat, 
# notable o excel·lent.
#
# 12-for-notes.sh
# --------------------------------------------------------
# 1) Valida que hi ha minim 1 argument
if [ $# -eq 0 ]; then
  echo "Error! num args"
  echo "Usage: $0 nota..."
  exit 1
fi

# 2) Iterar la llista d'arguments
for nota in $*
do 
  if ! [ $nota -ge 0 -a $nota -le 10 ]; then
    echo "Error: nota $nota no valida (0-10)" >> /dev/stderr
  else 
    if [ $nota -lt 5 ]; then
     echo "Suspes"
    elif [ $nota -lt 7 ]; then
     echo "Aprovat"
    else
     echo "Notable"
   fi
  fi
done
exit 0


