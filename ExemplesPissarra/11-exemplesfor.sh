# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Dir els dies que te un mes  
#
# Exemples bucles for
#
# 11-exemplesfor.sh
#
# --------------------------------------------------------

# 8) Llistar els logins

llistar=$(cut -d: -f1 /etc/passwd | sort)
num=1
for elem in $llistar
do
  echo "$num:$elem"
  ((num++))
done
exit 0

# 7) LListar numerant les linies
llistat=$(ls)
num=1
for elem in $llistat
do
  echo "$num:$elem"
  ((num++))
done
exit 0

# 6) Iterar pel resultat d'executar la ordre ls

llistat=$(ls)

for elem in $llistat
do
  echo "$elem"
done
exit 0

# 5) numerar arguments

num=1
for arg in $*
do
  echo "$num:$arg"
  ((num++))
done
exit 0

# 4) $@ expandeix $* no

for arg in "$*"
do
  echo "arg$"
done
exit 0

# 3) Iterar per la llista d'arguments

for arg in "$*"
do
  echo "arg$"
done
exit 0

# 2) Iterar noms

for nom in "pere" "marta" "anna" "pau"
do
  echo "$nom"
done
exit 0

# 1) Iterar noms
for nom in "pere" "marta" "anna" "pau"
do
  echo "$nom"
done
exit 0
