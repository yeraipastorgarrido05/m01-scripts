# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Dir els dies que te un mes  
#
# Programa mes:
# 	a) validar rep un arg
# 	b) validar mes [1-12]
# 	c) Xixa 
#
# 10-case-diames.sh
#
# --------------------------------------------------------
# 1) Valida que hi ha 1 argument
ERR_NARGS=1
ERR_ARGVL=2
if [ $# -ne 1 ]
then
  echo "Error! numero arguments incorrecte"
  echo "Usage: $0 mes"
  exit $EE_NARGS
fi

# 2) Valida mes
mes=$1
if [ $mes -ge 1 -o $mes -le 12 ];
then
  echo "Error, mes $mes no valid"
  echo "Mes no pren els valors del [1-12]"
  echo "Usage: $0 mes"
  exit $ERR_NARGS	

# 3) Xixa
case $mes in
  "2")
     dies=28;;
  "4"|"6"|"9"|"11")
     dies=30;;
  *)
     dies=31;;

esac
exit 0

