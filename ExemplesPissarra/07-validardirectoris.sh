# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Llistar el directori rebut
#
#	a) verificar rep un argument
#	b) verificar que es un directori
#
# 05-validarnota.sh
# --------------------------------------------------------
# 1) Valida que hi ha 1 argument
ERR_NARGS=1
EE_NODIR=2
if [ $# -ne 1 ]
then
  echo "Error! num args incorrecte"
  echo "Usage: $0 directori"
  exit $EE_NARGS
fi

# 2) Validar que es un directori
directori=$1
if [ -d $directori ]
then
  echo "Error: $1 no es un directori"
  echo "Usage: $0 directori"
  exit $ERR_NODIR

# 3) Mostra per pantalla el directori
else
  echo "$directori es un directori"
fi
exit 0
