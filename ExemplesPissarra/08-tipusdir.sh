# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Indicar si dir es: regular, dir, link o altra cosa
#
# 08-tipusdir.sh
# --------------------------------------------------------
# 1) Valida que hi ha 2 arguments
ERR_NARGS=1
EE_NODIR=2
if [ $# -ne 1 ]
then
  echo "Error! num args incorrecte"
  echo "Usage: $0 dir"
  exit $EE_NARGS
fi

# 2) Validar que es un directori
dir=$1
if [ ! -e $dir  ]; 
then
  echo "$dir no existeix"
  exit $ERR_NOEXIST

elif 


elif [ -d $directori ]
then
  echo "Error: $1 no es un directori"
  echo "Usage: $0 directori"
  exit $ERR_NODIR

# 3) Mostra per pantalla el directori
else
  echo "$directori es una altra cosa"
fi
exit 0

