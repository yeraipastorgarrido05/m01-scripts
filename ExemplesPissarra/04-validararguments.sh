# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Validar que te exactament 2 arguments i mostrar-los
#
# 04-validararguments.sh
# --------------------------------------------------------
# 1) Valida que hi ha 2 arguments
if [ $# -ne 2 ]
then
  echo "Error! num args incorrecte"
  echo "Usage: $0 nom cognom"
fi
# 2) xixa que el mostra
echo "nom: $1"
echo "cognom: $2"
exit 0

