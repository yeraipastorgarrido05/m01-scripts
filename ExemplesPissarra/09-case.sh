# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Exemple case
#
# 09-case.sh
# --------------------------------------------------------
# exemple vocals
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
    echo "$1 es laborable";;
  "ds"|"dm")
    echo "$1 es festiu";;
  *)
    echo "$1 no  es un dia";;

esac
exit 0


