# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Exemples while
#
# 13-exemples-while.sh
# --------------------------------------------------------
# 8) Procesar linia a linia un file
fileIn=$1
linia=0
while read -r line
do
  chars=$(echo $line | wc -c)
  echo "$linia: ($chars) $line" | tr 'a-z' 'A-Z'
  ((linia++))
done < $fileIn 
exit 0

# 7) Numerar i mostrar en majuscules
#    stdin
linia=0
while read -r line
do 
  echo "$linia: $line" | tr 'a-z' 'A-Z'
  ((linia++))
done
exit 0

# 6) Itera linia a linia fins a token
#    (per exemple FI)
TOKEN="FI"
linia=0
read -r line
while [ "$line" != $TOKEN ]
do
  echo " $linia: $line"
  read -r line
  ((linia++))
done 
exit 0

# 5) Numerar les linies rebudes
linia=0
while read -r line
do 
  echo " $linia: $line"
  ((linia++)
done
exit 0

# 4) Procesar stdin linia a linia
while read -r line
do 
  echo $line
done
exit 0

# 3) Iterar la llista d'arguments
#   (oju! usar normalment for)

while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift
done
exit 0

# 2) Comptador decrementa valor rebut
MIN=0
num=$1
while [ $num -ge $MIN ]
do
  echo -n "$num "
  ((num--))
done
exit 0

# 1) Mostrar numeros del 1 al 10
MAX=10
num=1
while [ $num -le $MAX  ]
do
  echo -n "$num"
  ((num++))
done
exit 0
