# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# programa dir
#
# rep un arg i es un directori i es llista
#
#
# 14a-llistar.sh
#
# --------------------------------------------------------
# 1) Valida que hi ha un argument

if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi
dir=$1

# 2) Valida que es un dir
dir=$1
if [ ! -d $dir ]
then
  echo "Error: $1 no es un directori"
  echo "Usage: $0 directori"
  exit $ERR_NODIR
fi

# 3) xixa:llistar

ls $dir
exit=0
