# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Validar nota: suspes, aprovat
#
#	a) rep un argument
#	b) es del 1 al 10
#
# 05-validarnota.sh
# --------------------------------------------------------
# 1) Valida que hi ha 2 arguments
ERR_NARGS=1
EE_NOTA=2
if [ $# -ne 1 ]
then
  echo "Error! num args incorrecte"
  echo "Usage: $0 nota"
  exit $EE_NARGS
fi

# 2) Validar que la nota es del 0 al 10 [0-10]
nota=$1
if ! [ $nota -ge 0 -a $nota -le 10 ]
then
  echo "nota pren valors del 0 al 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi
# 3) Xixa
nota=$1
if [ $nota -lt 5 ]
then
  echo "nota $nota esta suspes"
elif [ $nota -le 7 ]
then
  echo "nota $nota esta be"
elif [ $nota -le 9 ]
then
  echo "nota $nota es notable"
else
  echo "nota $nota es excel·lent"

fi
exit 0

