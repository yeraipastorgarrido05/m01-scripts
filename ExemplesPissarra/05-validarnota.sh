# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Validar nota: suspes, aprovat
#
#	a) rep un argument
#	b) es del 1 al 10
#
# 05-validarnota.sh
# --------------------------------------------------------
# 1) Valida que hi ha 2 arguments
ERR_NARGS=1
EE_NOTA=2
if [ $# -ne 1 ]
then
  echo "Error! num args incorrecte"
  echo "Usage: $0 nota"
  echo $EE_NARGS
fi

# 2) Validar que la nota es del 0 al 10 [0-10]
nota=$1
if ! [ $1 -ge 0 -a -le 10 ]
then
  echo "nota pren valors del 0 al 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi
# 3) Xixa
nota=$1
if [ $nota -lt 5 ]
then
  echo "nota $nota esta suspes"
else
  echo "nota $nota esta aprovat"

fi
exit 0

