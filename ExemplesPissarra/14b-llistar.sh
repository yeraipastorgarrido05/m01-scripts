# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# programa dir
#
# a) rep un arg i es un directori i es llista
# b) llistar enumerant els elements del dir
#
# 14b-llistar.sh
#
# --------------------------------------------------------
# 1) Valida que hi ha un argument

if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi
dir=$1

# 2) Valida que es un dir
dir=$1
if [ ! -d $dir ]
then
  echo "Error: $1 no es un directori"
  echo "Usage: $0 directori"
  exit $ERR_NODIR
fi

# 3) xixa:llistar

llista_dir=$(ls $dir)
num=0
for elem in $llista_dir
do
  echo "$num:$elem"
   ((num++))
done
   exit 0
