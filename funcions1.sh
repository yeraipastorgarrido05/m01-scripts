#! /bin/bash
# @Yerai Pastor Garrido
# funcions
# --------------------------------------------

function suma(){
  echo $(($1+$2))
  return 0
}

function showUserByLogin(){
 # 1) validar n args
 # 2) Validar login existeix
  login=$1
  line=$(grep "^$login:" /etc/passwd)
  if [ -z  "$line"  ];then
    echo "Error..."
    return 1
  fi
  uid=$(echo $line | cut -d: -f3)
  gid=$(echo $line | cut -d: -f4)
  shell=$(echo $line | cut -d: -f7)
  echo "login: $login"
  echo "uid: $uid"
  echo "gid: $gid"
  echo "shell: $shell"

}

function showUsersInGroupByGid(){
 # 1) Validar args
 # 2) Validar existeix group
 #      mostrar gname(gid)
 # 3) login, uid, shell <-- gid

 gname=$(grep "$^[^:]*:[^:]*":$gid:"" /etc/group | cut -d: -f1)

  if [ -z "$gname" ];then
    echo "Grup: $gname($gid)"

  fi
}



function showAllShells(){

  shellList=$(cut -d: -f7 /etc/passwd | sort | uniq)
  
  for shell in $shellList
  do
    echo "Shell: $shell"
    grep ":$shell$" /etc/passwd | cut -d: -f 1,3,4 | sed -r 's/^(.*)$/\t\1/'
  done
}


