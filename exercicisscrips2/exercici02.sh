# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
#
# Exercicis Scripts 2 : Exercici02.sh
#
# --------------------------------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
fi

num=0
for args in $*
do
  echo $args | egrep -q '.{3}' &>/dev/null
    if [ $? -eq 0 ]; then 
      num=$((num+1))
    fi
done
echo "$num"
exit 0
