# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Processar stdin cmostrant per stdout les línies numerades i en majúscules..
#
# Exercicis Scripts 4 : Exercici04.sh
#
# --------------------------------------------------------
num=1
#1) xixa
while read -r line
do
	echo "$num:$line" | tr 'a-z' 'A-Z'
		((num++))
done
exit 0

