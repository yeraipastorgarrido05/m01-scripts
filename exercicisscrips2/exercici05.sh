# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#
# Exercicis Scripts 5 : Exercici05.sh
#
# --------------------------------------------------------

while read -r line
do
	echo $line | grep -E "^.{,50}$"
done
exit 0

