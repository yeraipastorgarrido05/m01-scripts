# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Processar per stdin línies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia
# en format → T. Snyder.
#
# Exercicis Scripts 6 : Exercici06.sh
#
# --------------------------------------------------------
while read -r line
do
	nom=$(echo $line | cut -c1)
	cognom=$(echo $line | cut -d " " -f2)
	echo "$nom. $cognom"
done
exit 0