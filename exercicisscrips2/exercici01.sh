# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#
# Exercicis Scripts 2 : Exercici01.sh
#
# --------------------------------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
fi

for arg in $*
do
  echo $arg | grep -Eiw ".{4,}"

done
exit 0

