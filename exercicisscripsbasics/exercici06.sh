# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Fer un programa que rep com a arguments noms de dies de la setmana i mostra
# quants dies eren laborables i quants festius. Si l’argument no és un dia de la
# setmana genera un error per stderr.
# Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
#
# Exercici06.sh
#
# --------------------------------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

laborable=0
festiu=0
for dia in $*
do
  case $dia in
  dilluns|dimarts|dimecres|dijous|divendres) 
    ((laborables++));;
  dissabte|diumenge) 
    ((festius++));;
  *) 
    echo "dia: $dia no es valid" >> /dev/stderr;; 
  esac
done
echo "laborables: $laborables"
echo "festius: $festius"
exit 0
