# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Fer un programa que rep com a argument un número indicatiu del número màxim de
# línies a mostrar. El programa processa stdin línia a línia i mostra numerades un
# màxim de num línies.
#
# Exercici10.sh
#
# --------------------------------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

num=1
MAX=$1
while read -r line
do
  if [ "$num" -le $MAX ]; then
    echo "$num: $line"
  else
    echo "$line"
  fi
  num=$((num+1))
done

