# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Mostar els arguments rebuts línia a línia, tot numerànt-los.#
#
# Exercici2.sh
#
# --------------------------------------------------------

num=1
for arg in $*
do
  echo "$num: $arg"
  ((num++))
done
exit 0


