# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema
# (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per
# stderr.
#
# Exercici08.sh
#
# --------------------------------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

for usuari in $*
do	
  grep "^$usuari:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo $usuari
  else
    echo $usuari >> /dev/stderr
  fi	  
done

