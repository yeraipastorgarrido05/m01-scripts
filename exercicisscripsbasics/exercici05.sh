# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Mostrar linia a linia l'entrada estàndard, retallant nomes els primers 50 caràcters
#
# Exercici05.sh
#
# --------------------------------------------------------

while read -r line
do	
  echo $line | cut -c1-50
done

