# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Fer un programa que rep com a arguments números de més (un o més) i indica per
# a cada mes rebut quants dies té el més.
#
# Exercici4.sh
#
# --------------------------------------------------------

# 1) Valida que hi ha 1 argument
ERR_NARGS=1
ERR_ARGVL=2
if [ $# -eq 0 ]
then
  echo "Error! numero arguments incorrecte"
  echo "Usage: $0 mes"
  exit $EE_NARGS
fi

# 2) Valida mes
for mes in $*
do
if [ $mes -lt 1 -o $mes -gt 12 ]; then
  echo "Error: mes $mes no esta entre [1-12]" >> /dev/stderr 

else
case $mes in
	2) echo "28";;
	1|3|5|7|8|10|12) echo "31";;
	*) echo "30";;
esac
fi
done
exit 0

