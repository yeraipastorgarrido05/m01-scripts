# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el
# sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra
# per stderr.
#
# Exercici9.sh
#
# --------------------------------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

while read -r line
do
  grep -q "^$line:" /etc/passwd
  if [ $? -eq 0 ]; then
    echo $line
  else
    echo $line >> /dev/stderr
  fi
done

