# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Fer un comptador des de zero fins al valor indicat per l’argument rebut.
#
# Exercici3.sh
#
# --------------------------------------------------------

if [ $# -ne 1 ]
then
  echo "Error! num args incorrecte"
  echo "Usage: $0 arg"
fi

contador=0
final=$1

while [ $contador -le $final ]
do
  echo -n "$contador "
  ((contador++))
done
exit 0
0
