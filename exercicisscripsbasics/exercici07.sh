# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la
# mostra, si no no.
#
# Exercici07.sh
#
# --------------------------------------------------------

while read -r line
do
  num=$(echo "$line" | wc -c)
  if [ "$num" -gt 60 ]; then
    echo $line
  fi
done
exit 0
