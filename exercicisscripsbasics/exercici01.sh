# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# Mostrar l’entrada estàndard numerant línia a línia
#
# Exercici1.sh
#
# --------------------------------------------------------

linia=0
while read -r line
do 
  echo " $linia: $line"
  ((linia++))
done
exit 0

