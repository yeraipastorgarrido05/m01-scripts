# /bin/bash
# @ Yerai Pastor Garrido ASIX-M01
# Febrer de 2024
# 
# File conte matricules
#
# --------------------------------------------------------
# 1) Valida que hi ha minim 1 argument
if [ $# -eq 0 ]; then
  echo "Error! num args"
  echo "Usage: $0 nota..."
  exit 1
fi

status=0
fileIn=$1
while read -r line
do

   echo $line | grep -E "^[A-Z,a-z]{4}[0-9]{3}$"
   if [ $? -ne 0 ]; then
     echo "$line" >> /dev/strerr
     status=3
   fi
done < $fileIn
exit $status
