# /bin/bash
# exemples de funcions de disc
# @edt Abril 2024
# -----------------------------------------------------------
function fsize(){
  login=$1
  line=$(grep "^$login:" /etc/passwd)
  if [ -z "$line" ]; then
     echo "error..."
     return 1
  fi
  dirHome=$(echo $line | cut -d: -f6)
  echo $dirHome
  du -sh $dirHome

}
# calcula el fsize del home
function loginargs(){
  if [ $# -eq 0 ]; then
    echo "error..."
    return 1
  fi
  echo $*
  for login in $*
  do
     fsize $login
  done
}
#rep un argument que es un fitxer
#cada linia del fitxer té un login
function loginfile(){
  if [ ! -e $1 ]; then
    echo"error..."
    return 1
  fi
  fileIn=$1
  echo $fileIn
  while read -r login
  do
     fsize $line
  done < $fileIn
  }
#rep logins per la entrada starndard 
function loginstdin(){
  while read -r login
  do
     fsize $login
  done
}

function loginboth(){
  fileIn="/dev/stdin"
  if [ $# -eq 1]; then
    fileIn=$1
  fi
  while read -r login
  do
    fsize $login
  done < $fileIn

}

